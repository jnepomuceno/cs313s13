CS313 Project 1 Readme
Jonathan Nepomuceno
LUC
---
General notes on JUnit:
-assertEquals(expected,actual);

TestPerformance:
	// TODO run test and record running times for SIZE = 10, 100, 1000, 10000
	// which of the two lists performs better as the size increases?
ArrayList;

SIZE=10;
testLinkedListAddRemove(0.045s)
testArrayListAddREmove(0.041s)
testLinkedListAccess(0.015s)
testArrayListAccess(0.010s)

LinkedList total: 0.060s
ArrayList total: 0.051s

SIZE=100;
testLinkedListAddRemove(0.045s)
testArrayListAddRemove(0.061s)
testLinkedListAccess(0.034s)
testArrayListAccess(0.011s)

LInkedList total: 0.079s
ArrayList total: 0.072s

SIZE=1000;
testLinkedListAddRemove(0.045s)
testArrayListAddRemove(0.374s)
testLinkedListAccess(0.431s)
testArrayListAccess(0.012s)

LinkedList total: 0.476s
ArrayList total: 0.386s

SIZE=10000;
testLinkedListAddRemove(0.050s)
testArrayListAddRemove(3.508s)
testLinkedListAccess(6.128s)
testArrayListAccess(0.011s)

LinkedList total: 6.178s
ArrayList total: 3.519s


TestList:
public void setUp()...
//TODO also try with a LinkedList - does it make any difference?
After fixes as described below
ArrayList time: 0.013s
LinkedList time: 0.015s
Roughly equivalent performance

public void testSizeNonEmpty()...
//TODO fix the expected values in the assertions below
Fixed.

public void testContains()...
// TODO write assertions using
// list.contains(77)
// that hold before and after adding 77 to the list
Fixed.

public void testAddMultiple()...
//TODO fix the expected values in the assertions below
Fixed.

public void testAddMultiple2()...
// TODO fix the expected values in the assertions below
Fixed.

public void testRemoveObject()...

list.remove(5); // what does this method do?
list.remove(Integer.valueOf(5)); //what does this one do?

The first: Either removes the element at the specified index in list, or removes the first occurrence of 5.
Because the list both is larger than 6 and contains an element 5 (at index 4), it could be either.

The latter: Assumedly this is the case of the former; depending on the action of the prior removal, this could be either 77 or 6.
//Test 1 results: assertEquals(4, list.lastIndexOf(77)); expected <4> but was <3>.
For the first: It actually removes the element at the specified index. Why? Something about boolean syntax, casting..?
For the second case, couldn't really observe the actual action of the removal, so created a new assertion
assertEquals(5, list.get(4).intValue());
//Test 2 results: expected <5> but was <6>.
How about that. So the second will actually remove the specified element IF it's explicitly stated as an Integer value.
In summary:
list.remove(int index) - no casting.
list.remove(Object o) - will removethe specified element; BUT may require the casting as demonstrated. 

Expected assertion values fixed accordingly.

public void testContainsAll()..
		// TODO using containsAll and Arrays.asList (see above),
		// 1) assert that list contains all five different numbers added
		// 2) assert that list does not contain all of 11, 22, and 33

Implemented assertTrue and assertFalse, respectively (latter required an import).

public void testAddAll()...
		// TODO in a single statement using addAll and Arrays.asList,
		// add items to the list to make the following assertions pass
		// (without touching the assertions themselves)

list.addAll( Arrays.asList( 33, 77, 44, 77, 55, 77, 66) );

public void testRemoveAll()...
		// TODO in a single statement using removeAll and Arrays.asList,
		// remove items from the list to make the following assertions pass
		// (without touching the assertions themselves)
list.removeAll( Arrays.asList( 33, 44, 55, 66) );

public void testRetainAll()...
		// TODO in a single statement using retainAll and Arrays.asList,
		// remove items from the list to make the following assertions pass
		// (without touching the assertions themselves)
list.retainAll( Arrays.asList(77) );

public void testSet()...
		// TODO use the set method to change specific elements in the list
		// such that the following assertions pass
		// (without touching the assertions themselves)
Added set functions

public void testSubList()...
		// TODO fix the arguments in the subList method so that the assertion
		// passes
Fixed.

TestIterator.java

public void setUp()...
//TODO also try with a LinkedList - does it make any difference?

public void testNonempty()...
		// TODO fix the expected values in the assertions below
Fixed.

public void testRemove()...
...
i.remove(); // TODO what happens if you use list.remove(77)?
With i.remove(), the element last passed by the iterator is removed. With list.remove(77), it tries to find an element at INDEX 77, and returns an IndexOutOfBoundsException. (Similar situation occurred in TestList experimentations; list.remove(Integer.ValueOf(77)) would have worked.)

		// TODO using assertEquals and Arrays.asList (see above)
		// express which values are left in the list
assertEquals( Arrays.asList( 33, 44, 55, 66 ), list);
